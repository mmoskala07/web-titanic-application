# Variable	Definition	            Key
# survival 	Survival 	            0 = No, 1 = Yes
# pclass 	Ticket class 	        1 = 1st, 2 = 2nd, 3 = 3rd
# sex 	    Sex
# Age 	    Age in years
# sibsp 	# of siblings / spouses aboard the Titanic
# parch 	# of parents / children aboard the Titanic
# ticket 	Ticket number
# fare 	    Passenger fare
# cabin 	Cabin number
# embarked 	Port of Embarkation 	C = Cherbourg, Q = Queenstown, S = Southampton


class DataBaseTable:
    TICKET = "ticket"
    FAMILY = "family"
    PASSENGER = "passenger"


class DataBase:
    SURVIVED = "survived"
    TICKET_CLASS = "ticket_class"
    TICKET_NUMBER = "ticket_number"
    TICKET_FARE = "fare"
    NAME = "name"
    SEX = "sex"
    AGE = "age"
    SIBLINGS_SPOUSES_NUMBER = "sibling_spouse"
    PARENTS_CHILDREN_NUMBER = "parent_child"
    CABIN = "cabin"
    EMBARKED = "embarked"


class Survived:
    YES = 1
    NO = 0


class TicketClass:
    FIRST = 1
    SECOND = 2
    THIRD = 3


class Sex:
    MALE = "male"
    FEMALE = "female"


class EmbarkedPort:
    CHERBOURG = "C"
    QUEENSTOWN = "Q"
    SOUTHAMPTON = "S"

