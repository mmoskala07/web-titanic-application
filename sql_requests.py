from Database_defines import DataBase, DataBaseTable, Survived, Sex, TicketClass, EmbarkedPort
LOWER_RANGE_LIMIT = 0
UPPER_RANGE_LIMIT = 1


def sql_request_family_table(siblings_spouses_range, parents_children_range):
    """
    Generate SQL request for family table according to given parameters

    Parameters
    ----------
    siblings_spouses_range : tuple of int, None
        Range of of siblings / spouses aboard the Titanic
    parents_children_range : tuple of int, None
        Range of parents / children aboard the Titanic

    Returns
    -------
    SQL request as string

    Examples
    --------
    sql_request_family_table((1,2), (2, None))
    OUT: 'SELECT * FROM family WHERE 1 <= sibling_spouse AND sibling_spouse <= 2 AND 2 <= parent_child'
    """
    result = ""
    if isinstance(siblings_spouses_range, tuple) and isinstance(parents_children_range, tuple):
        result = _generate_sql_request(DataBaseTable.FAMILY,
                                       sibsp=siblings_spouses_range,
                                       parch=parents_children_range)
    return result


def sql_request_passenger_table(survived, age, sex):
    """
    Generate SQL request for passenger table according to given parameters

    Parameters
    ----------
    survived : bool, None
    age : tuple of int, None
    sex : string, None

    Returns
    -------
    SQL request as string

    Examples
    --------
    sql_request_passenger_table(Survived.YES, (0.5, 25), Sex.MALE)
    'SELECT * FROM passenger WHERE survived = 1 AND sex = 'male' AND 0.5 <= age AND age <= 25.0'
    """
    result = ""
    if (survived in (Survived.NO, Survived.YES, None)) and (sex in (Sex.MALE, Sex.FEMALE, None)) and isinstance(age, tuple):
        result = _generate_sql_request(DataBaseTable.PASSENGER,
                                       survived=survived,
                                       age=age,
                                       sex=sex)
    return result


def sql_request_ticket_table(ticket_class, ticket_fare, embarked):
    """
    Generate SQL request for ticket table according to given parameters

    Parameters
    ----------
    ticket_class : tuple of int
    ticket_fare : tuple of int
    embarked : char

    Returns
    -------
    SQL request as string

    Examples
    --------
    sql_request_ticket_table((TicketClass.SECOND, None), (100, None), EmbarkedPort.SOUTHAMPTON)
    'SELECT * FROM ticket WHERE embarked = 'S' AND 2 <= ticket_class AND 100.00 <= fare'
    """
    result = ""
    if (embarked in (EmbarkedPort.CHERBOURG, EmbarkedPort.QUEENSTOWN, EmbarkedPort.SOUTHAMPTON, None)) and \
       isinstance(ticket_fare, tuple) and isinstance(ticket_class, tuple):
        result = _generate_sql_request(DataBaseTable.TICKET,
                                       ticket_class=ticket_class,
                                       ticket_fare=ticket_fare,
                                       embarked=embarked)
    return result


def _generate_sql_request(table_name, survived=None, sex=None, embarked=None, age=None, ticket_class=None, ticket_fare=None, sibsp=None, parch=None):
    conditions = []
    if survived is not None:
        conditions.append("{:s} = {:d}".format(DataBase.SURVIVED, bool(survived)))

    if sex is not None:
        conditions.append("{:s} = '{:s}'".format(DataBase.SEX, sex))

    if embarked is not None:
        conditions.append("{:s} = '{:s}'".format(DataBase.EMBARKED, embarked))

    if age is not None:
        if age[LOWER_RANGE_LIMIT] is not None:
            conditions.append("{:.1f} <= {:s}".format(age[LOWER_RANGE_LIMIT], DataBase.AGE))
        if age[UPPER_RANGE_LIMIT] is not None:
            conditions.append("{:s} <= {:.1f}".format(DataBase.AGE, age[UPPER_RANGE_LIMIT]))

    if ticket_class is not None:
        if ticket_class[LOWER_RANGE_LIMIT] is not None:
            conditions.append("{:d} <= {:s}".format(ticket_class[LOWER_RANGE_LIMIT], DataBase.TICKET_CLASS))
        if ticket_class[UPPER_RANGE_LIMIT] is not None:
            conditions.append("{:s} <= {:d}".format(DataBase.TICKET_CLASS, ticket_class[UPPER_RANGE_LIMIT]))

    if ticket_fare is not None:
        if ticket_fare[LOWER_RANGE_LIMIT] is not None:
            conditions.append("{:.2f} <= {:s}".format(ticket_fare[LOWER_RANGE_LIMIT], DataBase.TICKET_FARE))
        if ticket_fare[UPPER_RANGE_LIMIT] is not None:
            conditions.append("{:s} <= {:.2f}".format(DataBase.TICKET_FARE, ticket_fare[UPPER_RANGE_LIMIT]))

    if sibsp is not None:
        if sibsp[LOWER_RANGE_LIMIT] is not None:
            conditions.append("{:d} <= {:s}".format(sibsp[LOWER_RANGE_LIMIT], DataBase.SIBLINGS_SPOUSES_NUMBER))
        if sibsp[UPPER_RANGE_LIMIT] is not None:
            conditions.append("{:s} <= {:d}".format(DataBase.SIBLINGS_SPOUSES_NUMBER, sibsp[UPPER_RANGE_LIMIT]))

    if parch is not None:
        if parch[LOWER_RANGE_LIMIT] is not None:
            conditions.append("{:d} <= {:s}".format(parch[LOWER_RANGE_LIMIT], DataBase.PARENTS_CHILDREN_NUMBER))
        if parch[UPPER_RANGE_LIMIT] is not None:
            conditions.append("{:s} <= {:d}".format(DataBase.PARENTS_CHILDREN_NUMBER, parch[UPPER_RANGE_LIMIT]))

    return _generate_sql_request_from_conditions(table_name, conditions)


def _generate_sql_request_from_conditions(table_name, conditions):
    if not conditions:
        request = "SELECT * FROM {:s}".format(table_name)
    else:
        request = "SELECT * FROM {:s} WHERE ".format(table_name)
        for idx, condition in enumerate(conditions):
            request += condition
            if idx + 1 != len(conditions):
                request += " AND "
    return request
