from flask import Flask, render_template, request
import requests
import json

from flask_table import Table, Col
from Database import *
import sql_requests
from Database_defines import *

app = Flask(__name__)
database = Database()

# Declare your table
class FamilyTable(Table):
    classes = ["table", "table-sm"]
    id = Col('Id')
    sibling_spouse = Col('sibling_spouse')
    parent_child = Col('parent_child')

class PassengerTable(Table):
    classes = ["table", "table-sm"]
    id = Col('id')
    id_family = Col('id_family')
    id_ticket = Col('id_ticket')
    name = Col('name')
    survived = Col('survived')
    sex = Col('sex')
    age = Col('age')

class TicketTable(Table):
    classes = ["table", "table-sm"]
    id = Col('id')
    ticket_class = Col('ticket_class')
    ticket_number = Col('ticket_number')
    fare = Col('fare')
    cabin = Col('cabin')
    embarked = Col('embarked')

@app.route('/')
def hello_world():
    return render_template("home.html")

@app.route('/family', methods=['GET', 'POST'])
def display_family():
    family_parameters = ["siblings_spouses_min", "siblings_spouses_max", "parents_children_min", "parents_children_max"]
    family_variables = [None, None, None, None]
    for index, parameter in enumerate(family_parameters):
        if request.form.get(parameter):
            family_variables[index] = int(request.form.get(parameter))
        else:
            family_variables[index] = None
    siblings_spouses_min = family_variables[0]
    siblings_spouses_max = family_variables[1]
    parents_children_min = family_variables[2]
    parents_children_max = family_variables[3]

    sql_request = sql_requests.sql_request_family_table((siblings_spouses_min, siblings_spouses_max),
                                                        (parents_children_min, parents_children_max))
    items_family = database.execute_sql_request(sql_request)
    table = FamilyTable(items_family)
    return render_template("home.html", table=table)

@app.route('/passenger', methods=['GET', 'POST'])
def display_passenger():
    if request.form.get("age_min"):
        age_min = int(request.form.get("age_min"))
    else:
        age_min = None

    if request.form.get("age_max"):
        age_max = int(request.form.get("age_max"))
    else:
        age_max = None

    if request.form.get("survived"):
        survived = bool(int(request.form.get("survived")))
    else:
        survived = None

    if request.form.get("sex"):
        sex = str(request.form.get("sex"))
    else:
        sex = None

    sql_request = sql_requests.sql_request_passenger_table(survived, (age_min, age_max), sex)
    items_passenger = database.execute_sql_request(sql_request)
    table = PassengerTable(items_passenger)
    return render_template("home.html", table=table)

@app.route('/ticket', methods=['GET', 'POST'])
def display_ticket():
    ticket_parameters = ["fare_min", "fare_max", "class_min", "class_max"]
    ticket_variables = [None, None, None, None]
    for index, parameter in enumerate(ticket_parameters):
        if request.form.get(parameter):
            ticket_variables[index] = int(request.form.get(parameter))
        else:
            ticket_variables[index] = None
    fare_min = ticket_variables[0]
    fare_max = ticket_variables[1]
    class_min = ticket_variables[2]
    class_max = ticket_variables[3]

    if request.form.get("embarked"):
        embarked = request.form.get("embarked")
    else:
        embarked = None

    sql_request = sql_requests.sql_request_ticket_table((class_min, class_max), (fare_min, fare_max), embarked)
    items_ticket = database.execute_sql_request(sql_request)
    table = TicketTable(items_ticket)
    return render_template("home.html", table=table)

@app.route('/figure', methods=['GET', 'POST'])
def display_figure():
    img = request.form.get("figure") + ".jpg"
    return render_template("home.html", img=img)

if __name__ == '__main__':
    app.run()
