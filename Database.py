from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy import ForeignKey, CheckConstraint
from sqlalchemy.orm import sessionmaker
import pandas as pd
import yaml
from Database_defines import *

# Variable:     # Definition        # Key
# survival        survival            0 - No, 1 - Yes
# pclass          Ticket class        1 - 1st, 2- 2nd, 3 - 3rd
# sex             sex                 male, female (uuu nietoleracja, uuu)
# age             age in years
# sibsp           no. of siblings / spouses aboard the Titanic
# parch           no. of parents / children aboard the Titanic
# ticket          Ticket number
# fare            Passenger fare
# cabin           Cabin number
# embarked        Port of Embarktion  C = Cherbourg, Q = Queenstown, S = Southampton


class Database:
    def __init__(self):
        with open("Database_login.yaml", "r") as file:
            db_info = yaml.load(file, Loader=yaml.FullLoader)
        db_login = "{:s}://{:s}:{:s}@{:s}:{:d}/{:s}".format(db_info["database_type"],
                                                            db_info["user"],
                                                            db_info["password"],
                                                            db_info["database_url"],
                                                            db_info["port"],
                                                            db_info["database_name"])
        self.db = create_engine(db_login)
        self.Base = declarative_base()
        self.imported_data = pd.read_csv("data/Combined_data.csv")
        print(self.imported_data)

    def create_schema_for_database(self):
        class Family(self.Base):
            __tablename__ = DataBaseTable.FAMILY
            __table_args__ = (
                CheckConstraint('sibling_spouse >= 0'),
                CheckConstraint('parent_child >= 0')
            )
            id = Column(Integer, primary_key=True)
            sibling_spouse = Column(Integer)
            parent_child = Column(Integer)

            def __repr__(self):
                return "<Family(id='{0}', sibling_spouse={1}, parent_child={2}>".format(self.id, self.sibling_Spouse,
                                                                                        self.parent_child)

        class Ticket(self.Base):
            __tablename__ = DataBaseTable.TICKET
            __table_args__ = (
                CheckConstraint('fare >= 0'),
                CheckConstraint('ticket_Class >=1 and ticket_Class <= 3')
            )
            id = Column(Integer, primary_key=True)
            ticket_class = Column(Integer)
            ticket_number = Column(String(30))
            fare = Column(Float)
            cabin = Column(String(30))
            embarked = Column(String(2))

            def __repr__(self):
                return "<Ticket(id='{0}', ticket_class={1}, ticket_number={2}, fare={3}, Cabin={4}, Embarked={5}>".format(
                    self.id,
                    self.ticket_class,
                    self.ticket_number,
                    self.fare,
                    self.cabin, self.embarked)

        class Passenger(self.Base):
            __tablename__ = DataBaseTable.PASSENGER
            __table_args__ = (
                CheckConstraint('length(name) > 0'),
                CheckConstraint("sex = 'male' or sex = 'female'"),
                CheckConstraint('age > 0'),
                CheckConstraint('survived = 0 or survived = 1')
            )
            id = Column(Integer, primary_key=True)
            id_family = Column(Integer, ForeignKey("family.id"))
            id_ticket = Column(Integer, ForeignKey("ticket.id"))
            name = Column(String(100))
            survived = Column(Integer)
            sex = Column(String(7))
            age = Column(Float)

            def __repr__(self):
                return "<Passenger(id='{0]', id_family='{0}, id_ticket='{2}', name='{3}', survived='{4}', sex='{5}', age='{6}'>".format(
                    self.id, self.id_family, self.id_ticket, self.name, self.survived, self.sex, self.age)

        self.Base.metadata.create_all(self.db)
        self.db.table_names()
        print("\nSchema For DataBase Finished")

    def import_data_into_dataBase(self):
        session_init = sessionmaker(bind=self.db)
        session = session_init()

        ## Put the data from the CSV file into the tables (variables):

        for index, row in self.imported_data.iterrows():
            name = row['Name']
            survived = row['Survived']
            sex = row['Sex']
            age = row['Age']
            sibling_spouse = row['SibSp']
            parent_child = row['Parch']
            ticket_number = row['Ticket']
            fare = row['Fare']
            cabin = row['Cabin']
            embarked = row['Embarked']
            ticket_class = row['Pclass']

        session.commit()

        family_list = self.imported_data[['SibSp', 'Parch']].drop_duplicates().reset_index().drop(columns=['index'])
        family_list.index.name = 'id'
        family_list = family_list.rename(columns={'SibSp': DataBase.SIBLINGS_SPOUSES_NUMBER,
                                                  'Parch': DataBase.PARENTS_CHILDREN_NUMBER})

        ticket_list = self.imported_data[
            ['Pclass', 'Ticket', 'Fare', 'Cabin', 'Embarked']].drop_duplicates().reset_index().drop(columns=['index'])
        ticket_list.index.name = 'id'
        ticket_list = ticket_list.rename(
            columns={'Pclass': DataBase.TICKET_CLASS,
                     'Ticket': DataBase.TICKET_NUMBER,
                     'Fare': DataBase.TICKET_FARE,
                     'Cabin': DataBase.CABIN,
                     'Embarked': DataBase.EMBARKED})

        passenger_list = self.imported_data[
            ['Name', 'Survived', 'Age', 'Sex', 'SibSp', 'Pclass']].drop_duplicates().reset_index().drop(
            columns=['index'])
        passenger_list.index.name = 'id'
        passenger_list = passenger_list.rename(
            columns={'Name': DataBase.NAME,
                     'Survived': DataBase.SURVIVED,
                     'Sex': DataBase.SEX,
                     'Age': DataBase.AGE,
                     'SibSp': 'id_{:s}'.format(DataBaseTable.FAMILY),
                     'Pclass': 'id_{:s}'.format(DataBaseTable.TICKET)})

        try:
            family_list.to_sql(DataBaseTable.FAMILY, self.db, if_exists='append')
        except:
            print("!!!An error during family list upload occured!")
        try:
            ticket_list.to_sql(DataBaseTable.TICKET, self.db, if_exists='append')
        except:
            print("!!!An error during ticket list upload occured!")
        try:
            passenger_list.to_sql(DataBaseTable.PASSENGER, self.db, if_exists='append')
        except:
            print("!!!An error during passenger list upload occured!")

        print("\n---------------"
              "\nImport Finished"
              "\n---------------")

    def drop_a_table(self, tableName):
        command = "DROP TABLE " + tableName + " CASCADE"
        try:
            result = self.db.execute(command)
            print("\nTable " + tableName + " dropped!\n" + str(result))
        except:
            print("!!!Error while executing ''" + str(command) + "'' !!!")

    def select_family(self):
        command = "SELECT * FROM {:s}".format(DataBaseTable.FAMILY)
        try:
            result = self.db.execute(command)
            return result
        except:
            print("!!!Error while executing ''" + str(command) + "'' !!!")

    def select_passenger(self):
        command = "SELECT * FROM {:s}".format(DataBaseTable.PASSENGER)
        try:
            result = self.db.execute(command)
            return result
        except:
            print("!!!Error while executing ''" + str(command) + "'' !!!")

    def select_ticket(self):
        command = "SELECT * FROM {:s}".format(DataBaseTable.TICKET)
        try:
            result = self.db.execute(command)
            return result
        except:
            print("!!!Error while executing ''" + str(command) + "'' !!!")

    def execute_sql_request(self, sql_request):
        try:
            return self.db.execute(sql_request)
        except:
            print("!!! Error while executing {} !!!".format(sql_request))


# For testing purposes only
if __name__ == "__main__":
    database = Database()
    database.drop_a_table("ticket")
    database.drop_a_table("family")
    database.drop_a_table("passenger")
    database.create_schema_for_database()
    database.import_data_into_dataBase()

    # Using sql_requests generator
    import sql_requests
    from Database_defines import *

    sql_request = sql_requests.sql_request_ticket_table((TicketClass.FIRST, TicketClass.FIRST), (100, 200), EmbarkedPort.SOUTHAMPTON)
    result = database.execute_sql_request(sql_request).fetchmany(50)
    print(sql_request)
    for row in result:
        print(row)

    sql_request = sql_requests.sql_request_passenger_table(Survived.YES, (0.0, 1), None)
    result = database.execute_sql_request(sql_request).fetchmany(50)
    print(sql_request)
    for row in result:
        print(row)
