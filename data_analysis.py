import matplotlib.pyplot as plt
import pandas as pd
from plot_defines import Colors
import seaborn as sns
import numpy as np


class DataAnalysis:
    def __init__(self):
        self.df = pd.read_csv("data/train.csv")

    def histogram_age(self):
        fig, ax = plt.subplots(1, 1)
        bins = self._get_bins("Age")
        ax.hist(self.df["Age"], color=Colors.blue_c, edgecolor=Colors.blue_e, bins=bins)
        ax.set_title("Travelling age groups")
        ax.set_ylabel("People")
        ax.set_xlabel("Age")
        ax.set_xticks(bins[::2])
        self._save_to_file("histogram_age")

    def survival_depending_on_age(self):
        self._survival_hist_plot("Age")
        self._save_to_file("survival_depending_on_age")

    def survival_depending_on_sex(self):
        self._survival_bar_plot("Sex")
        self._save_to_file("survival_depending_on_sex")

    def survival_depending_on_fare(self):
        self._survival_hist_plot("Fare")
        self._save_to_file("survival_depending_on_fare")

    def survival_depending_on_class(self):
        self._survival_bar_plot("Pclass")
        self._save_to_file("survival_depending_on_class")

    def survival_depending_on_embark(self):
        grid = sns.FacetGrid(self.df, row='Embarked', height=2.2, aspect=1.6)
        grid.map(sns.pointplot, 'Pclass', 'Survived', 'Sex', palette='deep')
        grid.add_legend()
        self._save_to_file("survival_depending_on_embark")

    def survival_depending_on_title(self):
        self.df["Title"] = self.df.Name.str.extract('([A-Za-z]+)\.', expand=False)
        self.df['Title'] = self.df['Title'].replace(['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Other')
        self.df['Title'] = self.df['Title'].replace('Mlle', 'Miss')
        self.df['Title'] = self.df['Title'].replace('Ms', 'Miss')
        self.df['Title'] = self.df['Title'].replace('Mme', 'Mrs')
        self._survival_bar_plot("Title")
        self._save_to_file("survival_depending_on_title")

    def _survival_hist_plot(self, criterion):
        # Create figure and axes, set title
        fig, ax = plt.subplots(2, 1)
        fig.suptitle("Alive and dead people in comparison to {:s}".format(criterion), fontsize=14)

        # Draw upper plot
        alive, dead = self._get_alive_and_dead(criterion=criterion)
        bins = self._get_bins(criterion)
        hist_data = ax[0].hist([alive, dead],
                               bins=bins,
                               label=["alive", "dead"],
                               color=[Colors.green_c, Colors.red_c])
        ax[0].legend()
        ax[0].set_ylabel("People")
        # That label is not displayed correctly so only label for lower plot will be enabled
        # ax[0].set_xlabel(criterion)
        ax[0].set_xticks(bins[::2])

        # Draw lower plot
        alive_groups = hist_data[0][0]
        dead_groups = hist_data[0][1]
        rate = (alive_groups / (alive_groups + dead_groups)) * 100

        bars = hist_data[1][:-1] + hist_data[1][1] / 2
        bar_width = 0.5*(bars[1] - bars[0])
        ax[1].bar(bars, rate,
                  width=bar_width,
                  color=Colors.blue_c,
                  edgecolor=Colors.blue_e)
        ax[1].set_ylabel("Survival rate [%]")
        ax[1].set_xlabel(criterion)
        ax[1].set_xticks(bins[::2])

    def _survival_bar_plot(self, criterion):
        # Create figure and axes, set title
        fig, ax = plt.subplots(2, 1)
        fig.suptitle("Alive and dead people in comparison to {:s}".format(criterion), fontsize=14)

        # Calc data
        alive, dead = self._get_alive_and_dead(criterion=criterion)
        bars = np.unique(self.df[criterion].dropna(), return_counts=False)
        _, alive_groups = np.unique(alive, return_counts=True)
        _, dead_groups = np.unique(dead, return_counts=True)

        # Draw upper plot
        x = np.arange(len(bars))
        ax[0].bar(x - 0.2, alive_groups, width=0.4, label="alive", color=Colors.green_c)
        ax[0].bar(x + 0.2, dead_groups, width=0.4, label="dead", color=Colors.red_c)
        ax[0].legend()
        ax[0].set_ylabel("People")
        # That label is not displayed correctly so only label for lower plot will be enabled
        # ax[0].set_xlabel(criterion)
        ax[0].set_xticks(x)
        ax[0].set_xticklabels(bars)

        # Draw lower plot
        rate = (alive_groups / (alive_groups + dead_groups)) * 100
        ax[1].bar(bars, rate, color=Colors.blue_c, edgecolor=Colors.blue_e)
        ax[1].set_ylabel("Survival rate [%]")
        ax[1].set_xlabel(criterion)
        ax[1].set_xticks(bars)

    def _get_alive_and_dead(self, criterion):
        alive, dead = [], []
        for idx, row in self.df.iterrows():
            if row["Survived"]:
                alive.append(row[criterion])
            else:
                dead.append(row[criterion])
        return alive, dead

    def _get_bins(self, criterion):
        if criterion == "Age":
            bins = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
        elif criterion == "Fare":
            bins = [0, 50, 100, 150, 200, 250, 300]
        else:
            bins = None
        return bins

    def _save_to_file(self, figure_name):
        plt.savefig("static\\{}.jpg".format(figure_name), dpi=300)


# For testing purposes only
if __name__ == "__main__":
    da = DataAnalysis()

    da.histogram_age()
    da.survival_depending_on_age()
    da.survival_depending_on_sex()
    da.survival_depending_on_fare()
    da.survival_depending_on_class()
    da.survival_depending_on_embark()
    da.survival_depending_on_title()

    print("Figures saved to 'static' folder")

